# Dependency Tree Parser for German Clinical Text

Tool coming from [http://macss.dfki.de/dependency_parser.html](http://macss.dfki.de/dependency_parser.html).

**Warning**

The conversion of the tool in a ELG compatible service is not well done. It has been done like that because of time constraints but this repo should not be used as an example.
