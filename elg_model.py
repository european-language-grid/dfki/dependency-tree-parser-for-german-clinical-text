from elg import FlaskService
from elg.model import Annotation, AnnotationsResponse, TextRequest
import subprocess
from conllu import parse
import os


class ELGService(FlaskService):
    @classmethod
    def to_annotations(cls, sentence, text, offset=0):
        tokens = []
        for tok in sentence:
            start_offset = text.find(tok["form"])
            tokens.append(
                Annotation(
                    **{
                        "start": offset + start_offset,
                        "end": offset + start_offset + len(tok["form"]),
                        "features": {
                            "words": [
                                {
                                    "form": tok["form"],
                                    "upos": tok["xpos"],
                                    "head": str(tok["head"]),
                                    "deprel": tok["deprel"],
                                }
                            ]
                        },
                    }
                )
            )
            offset += start_offset + len(tok["form"])
            text = text[start_offset + len(tok["form"]) :]
        return tokens, text, offset

    @classmethod
    def to_annotations_response(cls, sentences, text):
        offset = 0
        sentences_annotations = []
        tokens_annotations = []
        for sentence in sentences:
            start = offset
            tokens, text, offset = cls.to_annotations(sentence, text, offset=offset)
            sentences_annotations.append(Annotation(start=start, end=offset))
            tokens_annotations += tokens
        return AnnotationsResponse(
            annotations={
                "udpipe/sentences": sentences_annotations,
                "udpipe/tokens": tokens_annotations,
            }
        )

    @classmethod
    def run(cls, content):
        with open("input.txt", "w") as f:
            f.write(content)
        subprocess.call(
            [
                "java",
                "-cp",
                "stanford-corenlp-3.8.0.jar",
                "edu.stanford.nlp.pipeline.StanfordCoreNLP",
                "-annotators",
                "tokenize,ssplit,pos,depparse",
                "-Xmx2g",
                "-outputFormat",
                "conllu",
                "-outputExtension",
                ".conllu",
                "-file",
                "input.txt",
                "-outputDirectory",
                "./",
                "-depparse.model",
                "UD_German_Clinical_retrain_250_0.gz",
                "-pos.model",
                "german-ud.tagger",
            ]
        )
        os.remove("input.txt")
        with open("input.txt.conllu") as f:
            data = f.read()
        output = parse(data)
        os.remove("input.txt.conllu")
        return output

    def process_text(self, request: TextRequest):
        sentences = self.run(request.content)
        return self.to_annotations_response(sentences, request.content)


service = ELGService("")
app = service.app
